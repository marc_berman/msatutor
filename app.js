/***********************************************************************
 MSA Tutor RestAPI
 ============================================
 All server query calls are wrappe in our cutsom API for ease of use.
 ***********************************************************************/

/***********************************************************************
 BASE SETUP
 ============================================
 - call the packages we need
 - call express
 - define our app using express
 ***********************************************************************/

var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());

/***********************************************************************
 XSS - cross site scripting protection
 ============================================
 Set headers to allow all domain request for now.
 ***********************************************************************/
app.all('/*', function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');

    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.use('/api/v1/', require('./routes'));

/***********************************************************************
 HANDLE UNROUTED URLS
 ============================================
 Unrouted urls will throw an error
 ***********************************************************************/
app.all('*', function(req, res) {
    throw new Error("Bad request")
});


app.use(function(e, req, res, next) {
    if (e.message === "Bad request") {
        res.status(400).json({error: {output: e.message}});
    }
});

/***********************************************************************
 START THE SERVER
 ============================================
 ***********************************************************************/

app.set('port', process.env.PORT || 59898);
var server = app.listen(app.get('port'), function () {
    var urlHost = server.address().address;
    if (urlHost === "::") urlHost = "localhost";
    console.log(
        "==================================================" +
        "\nServer " + urlHost + ": " + server.address().port + " has successfully started." +
        "\n\nAll Calls need to be prefixed with /api/v1" +
        "\nEG. http://localhost:3000/api/v1/booking/add" +
        "\n==================================================");
});


