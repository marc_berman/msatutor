module.exports = function () {
    'use strict';
    var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://msatutor1%40gmail.com:msatutor123@smtp.gmail.com');

    return {
        registerEmail: function (data) {

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: '"MSA Tutor" <msatutor1@gmail.com>', // sender address
                to: data.Email, // list of receivers
                bcc: 'msatutor1@gmail.com',
                subject: 'MSA Tutor - Registration', // Subject line
                html: '<h2>Dear ' + data.FirstName + ' ' + data.LastName + '</h2>' +
                '<p>Welcome to the MSA Tutor program. Thank you for registering to be part of the program.</p>' +
                '<h3>You login credentials are as follows:</h3>' +
                '<p>Username: <strong>' + data.Username + '</strong><br>Password: <strong>' + data.Password + '</strong></p>' +
                '<p>Before you can login to your account an admin will have to verify you and your account. You will receive an Email once this process is complete.</p>' +
                '<p><strong>Kind Regards</strong><br>MSA Tutor Team</p>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
        },
        bookingUserEmail: function (data) {

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: '"MSA Tutor" <msatutor1@gmail.com>', // sender address
                to: data.Email, // list of receivers
                bcc: 'msatutor1@gmail.com',
                subject: 'MSA Tutor - Booking Receipt', // Subject line
                html: '<h2>Dear ' + data.Name + '</h2>' +
                '<p>Thank you for booking an appointment with one of our tutors.</p>' +
                '<p>This email servers as notice that we have received your booking and have notified the respective tutor.</p>' +
                '<p><strong>Kind Regards</strong><br>MSA Tutor Team</p>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
        },
        bookingTutorEmail: function (data, booking, time) {

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: '"MSA Tutor" <msatutor1@gmail.com>', // sender address
                to: data.Email, // list of receivers
                bcc: 'msatutor1@gmail.com',
                subject: 'MSA Tutor - Booking Request', // Subject line
                html: '<h2>Dear ' + data.FirstName + ' ' + data.LastName + '</h2>' +
                '<p>A booking request has been made with the following details:</p>' +
                '<p><strong>Name: </strong>' + booking.Name + '</p>' +
                '<p><strong>Email: </strong>' + booking.Email + '</p>' +
                '<p><strong>Session Type: </strong>' + booking.SessionType + '</p>' +
                '<p><strong>Time: </strong>' + time.Day + ' ' + time.Time + '</p><br>' +
                '<p><strong>Kind Regards</strong><br>MSA Tutor Team</p>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
        },
        activatedAccount: function (data) {
            var active = "";
            if (data.Activated == 1 || data.Activated == "1") {
                active = "Activated";
            }
            else {
                active = "Deactivated";
            }
            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: '"MSA Tutor" <msatutor1@gmail.com>', // sender address
                to: data.Email, // list of receivers
                bcc: 'msatutor1@gmail.com',
                subject: 'MSA Tutor - Account Updated', // Subject line
                html: '<h2>Dear ' + data.FirstName + ' ' + data.LastName + '</h2>' +
                '<p>Your account has been updated by the admin.</p>' +
                '<p>Your account status is ' + active + '.</p>' +
                '<p><strong>Kind Regards</strong><br>MSA Tutor Team</p>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: ' + info.response);
            });
        }
    }
};