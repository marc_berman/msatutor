Software License Agreement
========================================

Copyright (C) 2016 Marc Berman & Jeremy Paton. All rights reserved.

Unauthorized copying of this project or any of the files which form part of it, via any medium is strictly prohibited

Proprietary and confidential

Written by Marc Berman <marc@bermanz.co.za> and Jeremy Paton <jeremy@jesararts.net>, April 2016 - Present.

Dependencies
========================================
**Teamspeak-3-NodeJS-API includes code from the following projects, which have their own licenses:**
- [Node-Teamspeak](https://github.com/gwTumm/node-teamspeak/blob/master/LICENSE) (*<timklge@wh2.tu-dresden.de> wrote this thing. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return - Tim Kluge*) ("THE BEER-WARE LICENSE" (Revision 42)) 
