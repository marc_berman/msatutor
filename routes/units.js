module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var units = require('../middleware/units')();

    return {
        list: function (req, res) {

            units.list(function (data) {
                res.send(data);
            });
        },
        listBySchoolID: function (req, res) {

            units.listBySchoolID(req.params.schoolid, function (data) {
                res.send(data);
            });
        },
        addUnit: function (req, res) {
            units.addUnit(req.body, function (data) {
                res.send(data);
            });
        }
    }
};