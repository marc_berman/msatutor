module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var time = require('../middleware/time')();

    return {
        list: function (req, res) {

            time.list(req.params.userid, function (data) {
                res.send(data);
            });
        },
        listByID: function (req, res) {

            time.listByID(req.params.timeid, function (data) {
                res.send(data);
            });
        },
        add: function (req, res) {
            time.add(req.body, function (data) {
                res.send(data);
            });
        },
        update: function (req, res) {
            time.update(req.body, function (data) {
                res.send(data);
            });
        },
        delete: function (req, res) {
            time.delete(req.params.timeid, function (data) {
                res.send(data);
            });
        }
    }
};