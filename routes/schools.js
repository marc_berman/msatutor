module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var schools = require('../middleware/schools')();

    return {
        list: function (req, res) {
            schools.list(function (data) {
                res.send(data);
            });
        },
        addSchool: function (req, res) {
            schools.addSchool(req.body, function (data) {
                res.send(data);
            });
        }
    }
};