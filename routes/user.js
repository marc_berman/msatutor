module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var user = require('../middleware/user')();

    return {
        login: function (req, res) {
            user.login(req.body, function (data) {
                res.send(data);
            });
        },
        update: function (req, res) {
            user.update(req.body, function (data) {
                res.send(data);
            });
        },
        create: function (req, res) {
            user.create(req.body, function (data) {
                res.send(data);
            });
        },
        delete: function (req, res) {
            user.delete(req.params.userid, function (data) {
                res.send(data);
            });
        },
        list: function (req, res) {
            var startdate = req.body.startdate;
            var enddate = req.body.enddate;
            var activated = req.body.activated;
            var faculty = req.body.facultyid;
            var unit = req.body.unitid;
            user.list(activated, startdate, enddate, faculty, unit, function (data) {
                res.send(data);
            });
        },
        byusername: function (req, res) {
            user.byusername(req.params.username, function (data) {
                res.send(data);
            });
        },
        tutorTimes: function (req, res) {
            user.tutorTimes(req.params.userid, function (data) {
                res.send(data);
            });
        },
        tutors: function (req, res) {
            user.tutors(function (data) {
                res.send(data);
            });
        },
        tutorsByUnit: function (req, res) {
            user.tutorsByUnit(req.params.unitid, function (data) {
                res.send(data);
            });
        },
        admins: function (req, res) {
            user.admins(function (data) {
                res.send(data);
            });
        }
    }
};