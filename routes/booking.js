module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var booking = require('../middleware/booking')();

    return {
        list: function (req, res) {

            booking.list( function (data) {
                res.send(data);
            });
        },
        add: function (req, res) {
            booking.add(req.body, function (data) {
                res.send(data);
            });
        }
    }
};