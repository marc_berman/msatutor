module.exports = function () {
    'use strict';

    //Load phase MiddleWare
    var survey = require('../middleware/survey')();

    return {
        add: function (req, res) {
            survey.add(req.body, function (data) {
                res.send(data);
            });
        },
        update: function (req, res) {
            survey.update(req.body, function (data) {
                res.send(data);
            });
        },
        delete: function (req, res) {
            survey.delete(req.params.transactionid, function (data) {
                res.send(data);
            });
        },
        list: function (req, res) {
            survey.list(function (data) {
                res.send(data);
            });
        },
        listByTutor: function (req, res) {
            var facultyid = req.body.facultyid;
            var unitid = req.body.unitid;
            var userId = req.body.userid;
            var startdate = req.body.startdate;
            var enddate = req.body.enddate;
            survey.listByTutor(facultyid, unitid, userId, startdate, enddate, function (data) {
                res.send(data);
            });
        },
        sumtypes: function (req, res) {
            var startdate = req.body.startdate;
            var enddate = req.body.enddate;
            var facultyid = req.body.facultyid;
            var unitid = req.body.unitid;
            survey.sumtypes(startdate, enddate, facultyid, unitid, function (data) {
                res.send(data);
            });
        }
    }
};