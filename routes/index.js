var express = require('express');
var router = express.Router();

var booking = require('./booking')();
var user = require('./user')();
var survey = require('./survey')();
var units = require('./units')();
var schools = require('./schools')();
var time = require('./time')();

/*********************
 Booking Calls
 *********************/
router.get('/booking/list', booking.list);
router.post('/booking/add', booking.add);

/*********************
 User Calls
 *********************/
router.post('/user/login', user.login);
router.post('/user/update', user.update);
router.post('/user/create', user.create);
router.get('/user/delete/:userid', user.delete);
router.post('/user/list', user.list);
router.get('/user/byusername/:username', user.byusername);
router.get('/user/tutorstimes/:userid', user.tutorTimes);
router.get('/user/tutors', user.tutors);
router.get('/user/tutorsbyunit/:unitid', user.tutorsByUnit);
router.get('/user/admins', user.admins);

/*********************
 Survey Calls
 *********************/
router.post('/survey/add', survey.add);
router.post('/survey/update', survey.update);
router.get('/survey/delete/:transactionid', survey.delete);
router.get('/survey/list', survey.list);
router.post('/survey/listbytutor/', survey.listByTutor);
router.post('/survey/sumtypes', survey.sumtypes);

/*********************
 Units Calls
 *********************/
router.get('/units/list', units.list);
router.get('/units/listbyschool/:schoolid', units.listBySchoolID);
router.post('/units/add', units.addUnit);

/*********************
 Schools Calls
 *********************/
router.get('/schools/list', schools.list);
router.post('/schools/add', schools.addSchool);

/*********************
 Time Calls
 *********************/
router.post('/time/add', time.add);
router.post('/time/update', time.update);
router.get('/time/delete/:timeid', time.delete);
router.get('/time/list/:userid', time.list);
router.get('/time/listbyid/:timeid', time.listByID);

module.exports = router;