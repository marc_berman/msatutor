module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();

    var email = require('../config/email.js')();

    return {
        login: function (data, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE UserName=? AND Password=? AND u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.Activated = 1', [data.username, data.password], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "User has been logged in", data: rows});
                    }
                });
            });
        },
        update: function (data, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('UPDATE users SET FirstName=?, LastName=?, StudentNumber=?, Email=?, UserName=?, SchoolID=?, UnitID=?, Activated=? WHERE UserID=?', [data.FirstName, data.LastName, data.StudentNumber, data.Email, data.Username, data.SchoolID, data.UnitID, data.Activated, data.UserID], function (err, rows) {
                    connection.release();
                    email.activatedAccount(data);
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "User has been updated", data: rows});
                    }
                });
            });
        },
        create: function (data, callback) {

            var user = {
                FirstName: data.FirstName,
                LastName: data.LastName,
                Email: data.Email,
                Username: data.Username,
                Password: data.Password,
                SchoolID: data.Faculty,
                UnitID: data.Unit,
                UserLevel: 0,
                StudentNumber: data.StudentNumber
            };
            pool.getConnection(function (err, connection) {
                connection.query('INSERT INTO users SET ?', user, function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        console.log(rows.affectedRows, rows.affectedRows > 0);
                        if (rows.affectedRows > 0) {
                            email.registerEmail(user);
                        }

                        callback({status: "success", message: "User has been created", data: rows});
                    }
                });
            });
        },
        delete: function (userid, callback) {
            if (userid) {
                pool.getConnection(function (err, connection) {
                    connection.query('DELETE FROM users WHERE UserID = ?', [userid], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "User has been delete", data: rows});
                        }
                    });
                });
            }
            else {
                callback({status: "error", message: "userid was not included", data: null});
            }

        },
        list: function (activated, startdate, enddate, faculty, unit, callback) {
            pool.getConnection(function (err, connection) {
                if (startdate && enddate) {
                    //YYYY-MM-DD HH:mm:SS
                    //'2016-10-12 00:00:00' and '2016-10-12 23:59:00'
                    if (!activated && !faculty && !unit) {
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName", un.Code, u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.TimeStamp between ? and ?', [startdate, enddate], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    }
                    else if (activated && !faculty && !unit) {
                        if (activated == "yes") {
                            activated = 1
                        } else {
                            activated = 0
                        }
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName", un.Code, u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.Activated = ? AND u.TimeStamp between ? and ?', [activated, startdate, enddate], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (activated && faculty && !unit) {
                        if (activated == "yes") {
                            activated = 1
                        } else {
                            activated = 0
                        }
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName",  un.Code,u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.Activated = ? AND u.TimeStamp between ? and ? AND u.SchoolID = ?', [activated, startdate, enddate, faculty], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (activated && faculty && unit) {
                        if (activated == "yes") {
                            activated = 1
                        } else {
                            activated = 0
                        }
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName", un.Code, u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.Activated = ? AND u.TimeStamp between ? and ? AND u.SchoolID = ? AND u.UnitID = ?', [activated, startdate, enddate, faculty, unit], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (!activated && faculty && unit) {
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName", un.Code, u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.TimeStamp between ? and ? AND u.SchoolID = ? AND u.UnitID = ?', [startdate, enddate, faculty, unit], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (!activated && faculty && !unit) {
                        connection.query('SELECT u.UserLevel, u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name as "UnitName", un.Code, u.UserLevel, u.TimeStamp,u.Activated FROM users u, schools s, units un WHERE u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID AND u.TimeStamp between ? and ? AND u.SchoolID = ?', [startdate, enddate, faculty, unit], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    }

                    else {
                        callback({status: "error", message: "Unknown error has occurred", data: null});
                    }
                } else {
                    callback({status: "error", message: "startdate and enddate were not included", data: null});
                }

            });
        },
        byusername: function (username, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE UserName=? AND u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID', [username], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "User has been retrieved", data: rows});
                    }
                });
            });
        },
        tutors: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE u.UserLevel=0 AND u.SchoolID = s.SchoolID AND u.UnitID = un.UnitID', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Retrieved all tutors", data: rows});
                    }
                });
            });
        },
        tutorsByUnit: function (id, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE u.UserLevel=0 AND u.SchoolID = s.SchoolID AND u.UnitID = un.UnitID AND u.UnitID =?', [id], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Retrieved all tutors", data: rows});
                    }
                });
            });
        },
        tutorTimes: function (data, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT u.StudentNumber, t.TimeID, t.Day, t.Time FROM users as u, tutorTimes as t WHERE u.UserLevel = 0 AND t.UserID = u.UserID AND u.UserID =?', [data], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Retrieved tutors times", data: rows});
                    }
                });
            });
        },
        admins: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT  u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE u.UserLevel=1 AND u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Retrieved all admins", data: rows});
                    }
                });
            });
        }
    };
};