module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();

    return {
        list: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM units ORDER BY Code', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({
                            status: "error",
                            message: "An error occured when connecting to the db",
                            data: err
                        });
                    } else {
                        callback({status: "success", message: "Units were retrieved", data: rows});
                    }
                });

            });
        },
        listBySchoolID: function (id, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM units where SchoolID = ? ORDER BY Code', [id], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({
                            status: "error",
                            message: "An error occured when connecting to the db",
                            data: err
                        });
                    } else {
                        callback({status: "success", message: "Unit was retrieved", data: rows});
                    }
                });

            });
        },
        addUnit: function (data, callback) {
           var newUnit = {
                Name: data.Name,
                Code: data.Code,
                SchoolID: data.SchoolID
            };

            pool.getConnection(function (err, connection) {
                connection.query('INSERT INTO units SET ?', newUnit, function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Unit was added", data: rows});
                    }
                });
            });
        }
    };
};