module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();
    var email = require('../config/email.js')();

    return {
        list: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM booking', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({
                            status: "error",
                            message: "An error occured when connecting to the db",
                            data: err
                        });
                    } else {
                        callback({status: "success", message: "Bookings were retrieved", data: rows});
                    }
                });

            });
        },
        add: function (data, callback) {
            var newBooking = {
                Name: data.Name,
                Email: data.Email,
                SessionType: data.SessionType,
                SchoolID: data.Faculty,
                UnitID: data.Unit,
                UserID: data.UserID,
                TimeID: data.TimeID
            };

            pool.getConnection(function (err, connection) {
                    connection.query('INSERT INTO booking SET ?', newBooking, function (err, rows) {

                        if (err) {
                            connection.release();
                            callback({status: "error", message: "An error occured when connecting to the db", data: err});
                        } else {
                            if (rows.affectedRows > 0) {
                                email.bookingUserEmail(newBooking);

                                connection.query('SELECT u.StudentNumber, u.UserID, u.FirstName, u.LastName, u.Email, u.Username, s.SchoolID, s.SchoolName, un.UnitID, un.Name, u.UserLevel, u.TimeStamp FROM users u, schools s, units un WHERE u.UserID = ? AND u.SchoolID=s.SchoolID AND u.UnitID = un.UnitID', [newBooking.UserID], function (err2, rows2) {
                                    connection.query('SELECT * FROM `tutorTimes` WHERE TimeID = ?', [newBooking.TimeID], function (err3, rows3) {
                                        if (rows2.length > 0  && rows3.length > 0) {
                                            email.bookingTutorEmail(rows2[0], newBooking, rows3[0]);
                                        }
                                    });
                                });
                                connection.release();
                            }
                            callback({status: "success", message: "Bookings has been added", data: rows});
                        }
                    });
                }
            );
        }
    };
};