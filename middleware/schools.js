module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();

    return {
        list: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM schools ORDER BY SchoolName', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({
                            status: "error",
                            message: "An error occured when connecting to the db",
                            data: err
                        });
                    } else {
                        callback({status: "success", message: "Units were retrieved", data: rows});
                    }
                });

            });
        },
        addSchool: function (data, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('INSERT INTO schools SET ? ORDER BY SchoolName', data , function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Bookings has been added", data: rows});
                    }
                });
            });
        }
    };
};