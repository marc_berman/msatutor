module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();

    return {
        list: function (userid, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM tutorTimes WHERE UserID = ?', [userid], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Times were retrieved", data: rows});
                    }
                });

            });
        },
        listByID: function (timeid, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM tutorTimes WHERE TimeID = ?', [timeid], function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Times were retrieved", data: rows});
                    }
                });

            });
        },
        add: function (data, callback) {
            var newTime = {
                Day: data.Day,
                Time: data.Time,
                UserID: data.UserID
            };

            pool.getConnection(function (err, connection) {
                    connection.query('INSERT INTO tutorTimes SET ?', newTime, function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({status: "error", message: "An error occured when connecting to the db", data: err});
                        } else {
                            callback({status: "success", message: "Time has been added", data: rows});
                        }
                    });
                }
            );
        },
        update: function (data, callback) {
            pool.getConnection(function (err, connection) {
                    connection.query('UPDATE tutorTimes SET Day = ?, Time = ? WHERE TimeID = ?', [data.Day, data.Time, data.TimeID], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({status: "error", message: "An error occured when connecting to the db", data: err});
                        } else {
                            callback({status: "success", message: "Time has been updated", data: rows});
                        }
                    });
                }
            );
        },
        delete: function (timeid, callback) {
            pool.getConnection(function (err, connection) {
                    connection.query('DELETE FROM tutorTimes WHERE TimeID = ?', timeid, function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({status: "error", message: "An error occured when connecting to the db", data: err});
                        } else {
                            callback({status: "success", message: "Time has been deleted", data: rows});
                        }
                    });
                }
            );
        }
    };
};