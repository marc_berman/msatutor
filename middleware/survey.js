module.exports = function () {
    'use strict';
    var pool = require('../config/db.js')();

    return {
        add: function (data, callback) {
            var newSurvey = {
                StudentNum: data.StudentNum,
                FacultyID: data.Faculty,
                UnitID: data.Unit,
                TopicOfTheWeek: data.TopicOfTheWeek,
                Test: data.Test,
                Writing: data.Writing,
                ExamPrep: data.ExamPrep,
                Assignment: data.Assignment,
                CriticalThinking: data.CriticalThinking,
                Comment: data.Comment,
                Name: data.Name,
                UserID: data.UserID
            };

            pool.getConnection(function (err, connection) {
                connection.query('INSERT INTO survey SET ?', newSurvey, function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "Survey has been added", data: rows});
                    }
                });
            });
        },
        update: function (data, callback) {
            pool.getConnection(function (err, connection) {
                connection.query('UPDATE survey SET StudentNum = ?, FacultyID = ?, UnitID = ?, TopicOfTheWeek = ?, Test = ?, Writing = ?, ExamPrep = ?, Assignment = ?, CriticalThinking = ?,Comment = ?, Name = ? Where TransactionID = ?',
                    [data.StudentNum, data.FacultyID, data.UnitID, data.TopicOfTheWeek, data.Test, data.Writing, data.ExamPrep, data.Assignment, data.CriticalThinking, data.Comment, data.StudentName, data.TransactionID], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Survey has been updated", data: rows});
                        }
                    });
            });
        },
        delete: function (transactionid, callback) {
            if (transactionid) {
                pool.getConnection(function (err, connection) {
                    connection.query('DELETE FROM survey WHERE TransactionID = ?', [transactionid], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Transaction deleted", data: rows});
                        }
                    });
                });
            } else {
                callback({status: "error", message: "No transaction id attached", data: null});
            }

        },
        list: function (callback) {
            pool.getConnection(function (err, connection) {
                connection.query('SELECT * FROM survey', function (err, rows) {
                    connection.release();
                    if (err) {
                        callback({status: "error", message: "An error occured when connecting to the db", data: err});
                    } else {
                        callback({status: "success", message: "List of surveys retrieved", data: rows});
                    }
                });
            });
        },
        listByTutor: function (faculty, unit, userId, startdate, enddate, callback) {
            pool.getConnection(function (err, connection) {
                console.log(faculty, unit, userId, startdate, enddate);
                if (startdate && enddate) {
                    //YYYY-MM-DD HH:mm:SS
                    //'2016-10-12 00:00:00' and '2016-10-12 23:59:00'
                    if (!userId && !faculty && !unit) {
                        console.log("hello1");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ?', [startdate, enddate], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    }
                    else if (userId && !faculty && !unit) {
                        console.log("hello2");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.UserID = ? AND su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ?', [userId, startdate, enddate], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (userId && faculty && !unit) {
                        console.log("hello3");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.UserID = ? AND su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ? AND su.FacultyID = ?', [userId, startdate, enddate, faculty], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (userId && faculty && unit) {
                        console.log("hello4");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.UserID = ? AND su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ? AND su.FacultyID = ? and su.UnitID = ?', [userId, startdate, enddate, faculty, unit], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    } else if (!userId && faculty && unit) {
                        console.log("hello5");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ? AND su.FacultyID = ? and su.UnitID = ?', [startdate, enddate, faculty, unit], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    }
                    else if (!userId && faculty && !unit) {
                        console.log("hello5");
                        connection.query('SELECT su.TransactionID, su.StudentNum, su.FacultyID, su.UnitID, su.TopicOfTheWeek, su.Test, su.Writing, su.ExamPrep, su.Assignment, su.CriticalThinking, su.Comment, su.Name as StudentName, su.TimeStamp, su.UserID, sc.SchoolName, un.Name as UnitName, un.Code FROM survey su, schools sc, units un WHERE su.FacultyID = sc.SchoolID AND su.UnitID = un.UnitID AND su.TimeStamp between ? and ? AND su.FacultyID = ?', [startdate, enddate, faculty], function (err, rows) {
                            connection.release();
                            if (err) {
                                callback({
                                    status: "error",
                                    message: "An error occured when connecting to the db",
                                    data: err
                                });
                            } else {
                                callback({status: "success", message: "List of surveys retrieved", data: rows});
                            }
                        });
                    }
                    else {
                        callback({status: "error", message: "Unknown error has occurred", data: null});
                    }
                } else {
                    callback({status: "error", message: "startdate and enddate were not included", data: null});
                }
            });
        },
        sumtypes: function (startdate, enddate, facultyid, unitid, callback) {
            pool.getConnection(function (err, connection) {
                if (startdate && enddate && facultyid && unitid) {
                    connection.query('SELECT SUM(TopicOfTheWeek) AS "Topic Of The Week",  SUM(Test) AS "Test", SUM(Writing) AS "Writing",  SUM(ExamPrep) AS "Exam Prep", SUM(Assignment) AS "Assignment", SUM(CriticalThinking) AS "Critical Thinking" FROM survey WHERE TimeStamp between ? and ? AND FacultyID = ? AND UnitID = ?', [startdate, enddate, facultyid, unitid], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Sum of surveys retrieved", data: rows});
                        }
                    });
                }
                else if (startdate && enddate && facultyid) {
                    connection.query('SELECT SUM(TopicOfTheWeek) AS "Topic Of The Week",  SUM(Test) AS "Test", SUM(Writing) AS "Writing",  SUM(ExamPrep) AS "Exam Prep", SUM(Assignment) AS "Assignment", SUM(CriticalThinking) AS "Critical Thinking" FROM survey WHERE TimeStamp between ? and ? AND FacultyID = ?', [startdate, enddate, facultyid], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Sum of surveys retrieved", data: rows});
                        }
                    });
                }
                else if (startdate && enddate) {
                    connection.query('SELECT SUM(TopicOfTheWeek) AS "Topic Of The Week",  SUM(Test) AS "Test", SUM(Writing) AS "Writing",  SUM(ExamPrep) AS "Exam Prep", SUM(Assignment) AS "Assignment", SUM(CriticalThinking) AS "Critical Thinking" FROM survey WHERE TimeStamp between ? and ?', [startdate, enddate], function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Sum of surveys retrieved", data: rows});
                        }
                    });
                }
                else {
                    connection.query('SELECT SUM(TopicOfTheWeek) AS "Topic Of The Week",  SUM(Test) AS "Test", SUM(Writing) AS "Writing",  SUM(ExamPrep) AS "Exam Prep", SUM(Assignment) AS "Assignment", SUM(CriticalThinking) AS "Critical Thinking" FROM survey', function (err, rows) {
                        connection.release();
                        if (err) {
                            callback({
                                status: "error",
                                message: "An error occured when connecting to the db",
                                data: err
                            });
                        } else {
                            callback({status: "success", message: "Sum of surveys retrieved", data: rows});
                        }
                    });
                }
            });
        }
    };
};